import React, { Component } from 'react'

class Counter extends Component {
    state = { count: 1 }

    increment = () => {
        this.setState(prevState => ({
            count: prevState.count + 1,
        }))
    }

    decrement = () => {
        this.setState(prevState => ({
            count: prevState.count - 1,
        }))
    }
    render() {
        return (
            <React.Fragment>
                <span className={this.classes()}>{this.formatCount()}</span>
                <button
                    className={this.counterButtons()}
                    onClick={this.increment}
                >
                    +
                </button>
                <button
                    className={this.counterButtons()}
                    onClick={this.decrement}
                >
                    -
                </button>
            </React.Fragment>
        )
    }

    classes = () => {
        const { count } = this.state
        return `tag ${count === 0 ? ' is-warning' : 'is-info'} is-medium`
    }

    counterButtons = () => `tag is-primary is-medium`

    formatCount = () => {
        const { count } = this.state
        return count === 0 ? 'Zero' : count
    }
}

export default Counter
